import { Pipe, PipeTransform } from '@angular/core';
import { Ntf } from 'src/app/io/ntf';


@Pipe({
  name: 'prettyprint'
})
export class PrettyPrintPipe implements PipeTransform {
  transform(val: Ntf) {
    console.log(val)
    let resp = "";
    Object.keys(val).forEach(x => resp = resp + x + ":" + val[x] + "<br>");
    return resp.replace(";", "").replace(",", "");
  }
}
