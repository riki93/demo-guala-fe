import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ntf } from 'src/app/io/ntf';
import { NtfService } from 'src/app/service/ntf.service';
import { BaseComponent } from '../abstract/base.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends BaseComponent implements OnInit {

  loaded = false;
  ntfListView: Array<Ntf>;
  ntfList: Array<Ntf>;

  pageList: Array<number> = new Array();
  pageSize = 2;
  activePage = 1;

  constructor(private ntfService: NtfService, private router: Router) { super() }

  ngOnInit(): void {
    this.subscription['findAll'] = this.ntfService.findAll().subscribe(res => {
      this.loaded = true;
      const totalPage = Math.ceil(res.length / this.pageSize)
      for (let i = 0; i < totalPage; i++) {
        this.pageList.push(i + 1);
      }
      this.ntfList = res;
      this.ntfListView = this.paginate(res, this.pageSize, this.activePage);
    }, err => {
      console.log(err)
    })
  }

  nextPage() {
    this.activePage = this.activePage + 1;
    this.ntfListView = this.paginate(this.ntfList, this.pageSize, this.activePage)
  }

  prevPage() {
    this.activePage = this.activePage - 1;
    this.ntfListView = this.paginate(this.ntfList, this.pageSize, this.activePage)
  }

  pageclick(page: number) {
    this.activePage = page;
    this.ntfListView = this.paginate(this.ntfList, this.pageSize, page)
  }


  paginate(array, page_size, page_number) {
    return array.slice((page_number - 1) * page_size, page_number * page_size);
  }

  navigate(id: number) {
    this.router.navigateByUrl("detail/" + id);
  }



}
