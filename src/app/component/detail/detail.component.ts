import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Ntf } from 'src/app/io/ntf';
import { NtfService } from 'src/app/service/ntf.service';
import { BaseComponent } from '../abstract/base.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent extends BaseComponent implements OnInit {

  constructor(private route: ActivatedRoute, private ntfservice: NtfService) {
    super();
  }

  id: number;
  element: Ntf;

  ngOnInit(): void {
    this.subscription['routing'] = this.route.params.subscribe(params => {
      if (!!params['id']) {
        this.id = +params['id'];
        this.subscription['ntf'] = this.ntfservice.detail(this.id).subscribe(x => this.element = x, err => console.error(err))
      }
    });
  }

}
