import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';


@Injectable()
export abstract class BaseComponent implements OnDestroy {

    public subscription: { [x: string]: Subscription } = {};

    constructor() {
    }
    ngOnDestroy(): void {
        Object.keys(this.subscription).forEach((key: string) => {
            this.subscription[key].unsubscribe();
        });
    }


}
