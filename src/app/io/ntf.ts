export interface Ntf {
    id: number;
    name: string;
    volume: string;
    volume_USD: string;
    market_Cap: string;
    market_Cap_USD: string;
    sales: string;
    floor_Price: string;
    floor_Price_USD: string;
    average_Price: string;
    average_Price_USD: string;
    owners: string;
    assets: string;
    owner_Asset_Ratio: string;
    category: string;
    website: string;
    logo: string;
}