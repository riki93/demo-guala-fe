import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Ntf } from '../io/ntf';

@Injectable({
  providedIn: 'root'
})
export class NtfService {

  configUrl = environment.backend + '/ntf';

  constructor(private http: HttpClient) { }

  findAll(): Observable<Array<Ntf>> {
    return this.http.get<Array<Ntf>>(this.configUrl + "/findAll");
  }

  detail(id: number): Observable<Ntf> {
    return this.http.get<Ntf>(this.configUrl + "/" + id);
  }

}
